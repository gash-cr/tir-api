# TIR API
NodeJS application that serves the necessary endpoints in order to manage Gash's TIR system.

## Requirements
To build and run this app locally you will need a few things:

- [MongoDB](https://docs.mongodb.com/manual/installation/) 

- Development dependencies:
```bash
npm i -g nodemon
npm i -g ts-node
npm i -g typescript

```

- Production dependencies:
```bash
npm i -g pm2
pm2 install typescript
```

## Installation
```bash
npm i
```

## Serving locally
```bash
npm run dev
```
The developer mode will watch your changes then will transpile the TypeScript code and re-run the node application automatically.

## App skeleton
```
root/
├── src
│   ├── config
│   │   ├── connection.ts
│   │   ├── cron.ts
│   │   └── middleware.ts
│   ├── controllers
│   │   └── UserController.ts
│   ├── env
│   │   ├── defaults.ts
│   │   ├── development.ts
│   │   ├── index.ts
│   │   └── production.ts
│   ├── index.ts
│   ├── interfaces
│   │   └── ServerInterface.ts
│   ├── models
│   │   └── UserModel.ts
│   ├── router
│   │   ├── UserRouter.ts
│   │   └── routes.ts
│   ├── server.ts
│   └── serverHandlers.ts
├── tsconfig.json
├── LICENSE
├── nodemon.json
├── package-lock.json
├── package.json
```

## Example requests using CURL:
Create a user:

```bash
curl -X POST \
  http://localhost:3000/users \
  -H 'content-type: application/x-www-form-urlencoded' \
  -d 'name=test&email=test%40gmail.com'
```

Get a user:

```bash
curl -X GET \
  'http://localhost:3000/users?name=test&email=test%40gmail.com' \
  -H 'content-type: application/x-www-form-urlencoded' \
  -d 'name=checha1&email=checha1%40gmail.com'
```

## Deploying to Heroku
In order to deploy the app to Heroku make sure that the [mLab](https://elements.heroku.com/addons/mongolab) add on is installed, also verify that the following Config Vars are set:

* JWT_SECRET (used to sign/verify JWT tokens)
* MONGODB_URI (DB URL)
* MONGODB_DB_MAIN (DB name)

Then just push the app to the Heroku remote.
