// test config

export const envConfig: any = {
  database: {
    MONGODB_URI: process.env.MONGODB_URI || 'mongodb://localhost/',
    MONGODB_DB_MAIN: process.env.MONGODB_DB_MAIN || 'tirApiTest'
  },
  JWT_SECRET: process.env.JWT_SECRET || 'mySecret'
};
