import RevisionController from '../controllers/RevisionController';
import { Router } from 'express';
/**
 * @class RevisionRouter
 */
export default class RevisionRouter {
  public router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  public routes(): void {
    this.router.get('/', RevisionController.getRevisions);
    this.router.get('/search', RevisionController.getRevisionBy);
    this.router.get('/:id', RevisionController.getRevisionById);
    this.router.get('/by/:shipper', RevisionController.getRevisionsByShipper);
    this.router.post('/', RevisionController.createRevision);
  }
}
