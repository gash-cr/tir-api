import * as express from 'express';
import { IServer } from '../interfaces/ServerInterface';
import UserRouter from './UserRouter';
import AuthRouter from './AuthRouter';
import RevisionRouter from './RevisionRouter';
import ContainerRouter from './ContainerRouter';

export default class Routes {
  /**
   * @param  {IServer} server
   * @returns void
   */
  static init(server: IServer): void {
    const router: express.Router = express.Router();

    server.app.use('/', router);
    // AUTH
    server.app.use('/auth', new AuthRouter().router);
    // users
    server.app.use('/users', new UserRouter().router);
    // revisions
    server.app.use('/revision', new RevisionRouter().router);
    // containers
    server.app.use('/container', new ContainerRouter().router);
  }
}
