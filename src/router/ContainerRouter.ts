import ContainerController from '../controllers/ContainerController';
import { Router } from 'express';
/**
 * @class ContainerRouter
 */
export default class ContainerRouter {
  public router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  public routes(): void {
    this.router.get('/', ContainerController.getContainers);
    this.router.get('/:id', ContainerController.getContainerById);
    this.router.post('/', ContainerController.createContainer.bind(ContainerController));
  }
}
