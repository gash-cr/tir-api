import UserController from '../controllers/UserController';
import { Router } from 'express';
import AuthController from '../controllers/AuthController';
/**
 * @class UserRouter
 */
export default class UserRouter {
  public router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  public routes(): void {

    this.router.get('/', UserController.getUsers);
    this.router.get('/:id', AuthController.authRequired, UserController.getUserById);
    this.router.post('/', UserController.createUser);
    this.router.post('/password/:id', AuthController.authRequired, UserController.updatePassword);
    this.router.delete('/', UserController.deleteUser);
    this.router.put('/', UserController.updateUser);
  }
}
