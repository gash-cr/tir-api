import RevisionModel, { Revision } from '../models/RevisionModel';
import * as express from 'express';
import {
  PaginateOptions,
  PaginateResult
} from 'mongoose';

class RevisionController {
  createRevision = (data: any) :any => {
    let revision: any = data.revision;

    return RevisionModel
      .create({
        createdAt: revision.createdAt,
        user: revision.user,
        shipperName: data.shipperName,
        companyName: data.companyName,
        containerCode: data.containerCode,
        boatName: revision.boatName,
        travelNumber: revision.travelNumber,
        shippingDetail: revision.shippingDetail,
        shipping: revision.shipping,
        name: revision.name,
        chassis: revision.chassis,
        chassisType: revision.chassisType,
        truck: revision.truck,
        prefix: revision.prefix,
        driver: revision.driver,
        guideNumber: revision.guideNumber,
        containerType: revision.containerType,
        marks: [
          revision.customsMark1,
          revision.customsMark2,
          revision.customsMark3,
          revision.customsMark4,
          revision.customsMark5,
          revision.customsMark6
        ],
        measure: revision.measure,
        origin: revision.origin,
        destiny: revision.destiny,
        genSet: revision.genSet,
        chassisDetail: revision.chassisDetail,
        tiresCondition: revision.tiresCondition,
        tires: [revision.tire1, revision.tire2, revision.tire3, revision.tire4, revision.tire5, revision.tire6],
        part: revision.part,
        observations: revision.observations,
        damages: revision.damages
      })
      .then((revision: Revision) => {
        return revision;
      })
      .catch((error: Error) => {
        throw error.message;
      });
  }

  getRevisions = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    let options = this.setPaginationOptions(req);

    RevisionModel.paginate({}, options, (err: any, value: PaginateResult<Revision>) => {
      if (err) {
        return res.status(500).send(err);
      }

      return res.json(value);
    })
  }

  getRevisionBy = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const condition = req.query.condition;
    const query = {$or: [
      { truck: condition },
      { chassis: condition },
      { containerCode: condition }
    ]};

    let options = this.setPaginationOptions(req);

    RevisionModel.paginate(query, options, (err: any, value: PaginateResult<Revision>) => {
      if (err) {
        return res.status(500).send(err);
      }
      return res.json(value);
    })
  }

  private setPaginationOptions = (req: express.Request) => {
    const page = req.query.pageNo;
    const size = req.query.size;
    const descending = true;
    const options: PaginateOptions = {};
    options.select = `createdAt user companyName boatName shipperName containerCode travelNumber shippingDetail shipping name chassis chassisType truck prefix driver
      guideNumber containerType marks measure origin destiny genSet chassisDetail tiresCondition tires part observations damages`;
    options.sort = { createdAt: (descending ? -1 : 1) };
    options.page = page;
    options.offset = page * size;
    options.limit = req.query.size;

    return options;
  }

  getRevisionById = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    let idToFind: string = req.params.id;

    RevisionModel
      .findById(idToFind)
      .then(revision => {
        res.status(200).json(revision);
      })
      .catch((error: Error) => next(error));
  }

  getRevisionsByShipper = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const shipper: string = req.params.shipper;
    RevisionModel
      .find({ shipperName: shipper })
      .then(revision => {
        res.status(200).json(revision);
      })
      .catch((error: Error) => next(error));
  }
}

export default new RevisionController();