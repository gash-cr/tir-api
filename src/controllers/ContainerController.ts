import ContainerModel, { IContainerModel } from '../models/ContainerModel';
import * as express from 'express';
import RevisionController from './RevisionController';
import Logger from '../config/logger';

class ContainerController {

  public createContainer(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const data :Request = req.body;

    RevisionController.createRevision(data)
      .then((revision) => {
        this.addRevisionToContainer(revision, data);
      })
      .then((container) => {
        res.status(200).json(container);
      })
      .catch((error: Error) => {
        res.status(500).json({ error });
      });
  }

  private addRevisionToContainer(revision: any, data: any) :any {
    data.containerCode = data.containerCode.replace(/  *|-/g, '').toUpperCase();

    return ContainerModel.findOne({ containerCode: data.containerCode })
      .then((containerDB) => {
        if (!containerDB) {
          ContainerModel.create({
            containerCode: data.containerCode,
            shipperName: data.shipperName,
            revision
          }).then((container: IContainerModel) => container)
            .catch((error: Error) => (Logger.log(error.message, 'error')));
        } else {
          containerDB.revision.push(revision);
          containerDB.save();
        }

        return containerDB;
      }).catch((error: Error) => Logger.log(error.message, 'error'));
  }

  public getContainers(req: express.Request, res: express.Response, next: express.NextFunction): void {
    ContainerModel
      .find({}, 'containerCode shipperName revision')
      .then(containers => {
        res.status(200).json(containers);
      })
      .catch((error: Error) => next(error));
  }

  public getContainerById(req: express.Request, res: express.Response, next: express.NextFunction): void {
    let idToFind: string = req.params.id;

    ContainerModel
      .findById(idToFind)
      .then(container => {
        res.status(200).json(container);
      })
      .catch((error: Error) => next(error));
  }
}

export default new ContainerController();
