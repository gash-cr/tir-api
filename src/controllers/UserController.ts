import UserModel, { IUserModel } from '../models/UserModel';
import * as express from 'express';
import UserHelper from '../helpers/UserHelper';

class UserController {

  public createUser(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const { username, isAdmin, password, name, shippingCompany } = req.body;

    UserModel
      .create({
        username: username,
        isAdmin: isAdmin,
        passwordHash: password,
        name: name,
        shippingCompany: shippingCompany,
        isPasswordNew: true
      })
      .then((user: IUserModel) => {
        res.status(200).json(UserHelper.removeSensitiveData(user));
      })
      .catch((error: Error) => next(error));
  }

  public deleteUser(req: express.Request, res: express.Response, next: express.NextFunction): void {

    UserModel
      .findByIdAndRemove(req.body._id)
      .then((user: IUserModel) => {
        res.status(200).json(UserHelper.removeSensitiveData(user));
      })
      .catch((err: Error) => next(err));
  }

  public updateUser(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const { _id, username, name, isAdmin, email, password, shippingCompany, isPasswordNew } = req.body;

    UserModel.findById(_id, (err, user) => {
      if (err) {
        return false;
      }
      user.username = username;
      user.name = name;
      user.isAdmin = isAdmin;
      user.email = email;
      user.passwordHash = (isPasswordNew) ? password : user.passwordHash;
      user.shippingCompany = shippingCompany;
      user.isPasswordNew = isPasswordNew;
      user.save();
    })
      .then((user: IUserModel) => {
        res.status(200).json(UserHelper.removeSensitiveData(user));
      })
      .catch((err: Error) => next(err));
  }

  public getUsers(req: express.Request, res: express.Response, next: express.NextFunction): void {

    UserModel
      .find({}, '_id username name isAdmin email shippingCompany')
      .then(users => {
        res.status(200).json(users);
      })
      .catch((error: Error) => next(error));
  }

  public getUserById(req: express.Request, res: express.Response, next: express.NextFunction): void {

    let idToFind: string = req.params.id;

    UserModel
      .findById(idToFind)
      .then(user => {
        res.status(200).json(UserHelper.removeSensitiveData(user));
      })
      .catch((error: Error) => next(error));
  }

  public updatePassword(req: express.Request, res: express.Response, next: express.NextFunction): void {

    const idParam: string = req.params.id;
    const requestingUser: any = (<any>req).user;
    const userToUpdate: any = {
      id: idParam === 'me' ? requestingUser._id : idParam
    };

    if (idParam === 'me' || requestingUser.isAdmin) {
      // Update pass
    } else {
      // Not authorized!
    }
  }
}

export default new UserController();
