import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import { default as config } from '../env/index';
import UserModel, { IUserModel } from '../models/UserModel';
import UserHelper from '../helpers/UserHelper';

class AuthController {
  public login(req: express.Request, res: express.Response, next: express.NextFunction): void {
    const { username, password } = req.body;

    UserModel.findOne({ username })
      .then((user: IUserModel) => {
        if (user) {
          if (user.comparePassword(password)) {
            res.status(200).json({ token: jwt.sign((<any>UserHelper.removeSensitiveData(user)).toJSON(), config.envConfig.JWT_SECRET),
                                    isAdmin: user.isAdmin, shippingCompany: user.shippingCompany });
          } else {
            res.status(401).json({
              reason: 'Incorrect password'
            });
          }
        } else {
          res.status(404).json({
            reason: 'User not found'
          });
        }
      })
      .catch((error: Error) => next(error));
  }

  public authRequired(req: express.Request, res: express.Response, next: express.NextFunction): void {
    if (req['user']) {
      next();
    } else {
      let error: Error = new Error('unauthorized!');

      error['code'] = 401;
      next(error);
    }
  }
}

export default new AuthController();
