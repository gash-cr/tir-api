import { CronJob } from 'cron';
import Logger from './logger';

const TEST_CRON_INTERVAL: string = '* 1 * * * *';

/**
 * @class Cron
 */
export default class Cron {
  private static testCron(): void {
    new CronJob(TEST_CRON_INTERVAL, (): void => {
      Logger.log('Hello, I am Cron! Please see ../config/cron.ts', 'info');
    },
      null,
      true);
  }

  // init
  static init(): void {
    Cron.testCron();
  }
}
