import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import * as helmet from 'helmet';
import * as jwt from 'jsonwebtoken';
import { IServer } from '../interfaces/ServerInterface';
import { default as config } from '../env/index';
import * as morgan from 'morgan';

export default class Middleware {
  static init(server: IServer): void {

    // express middleware
    server.app.use(morgan('[:date[iso]] :remote-addr | ":method :url" :status :response-time | ms'));
    server.app.use(bodyParser.urlencoded({ extended: true, limit: '15mb' }));
    server.app.use(bodyParser.json({limit: '15mb'}));
    server.app.use(cookieParser());
    server.app.use(compression());
    server.app.use(helmet());
    server.app.use(cors());

    // cors
    server.app.use((req, res, next) => {
      res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS ');
      res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With,' +
        ' Content-Type, Accept,' +
        ' Authorization,' +
        ' Access-Control-Allow-Credentials'
      );
      res.header('Access-Control-Allow-Credentials', 'true');

      if (req.headers && req.headers.authorization && (req.headers.authorization as string).split(' ')[0] === 'JWT') {
        jwt.verify((req.headers.authorization as string).split(' ')[1], config.envConfig.JWT_SECRET, (err, decode) => {
          if (err) {
            req['user'] = undefined;
            next();
          } else {
            req['user'] = decode;
            next();
          }
        });
      } else {
        req['user'] = undefined;
        next();
      }
    });
  }
}
