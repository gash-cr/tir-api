import { IServer } from '../interfaces/ServerInterface';
import * as express from 'express';
import Logger from './logger';

export default class ErrorHandler {
  static init(server: IServer): void {
    server.app.use((err, req, res, next) => {
      if (err['code']) {
        this.handleCodeError(err, req, res, next);
      } else if (err['name'] && err['name'] !== 'Error') {
        this.handleNameError(err, req, res, next);
      } else {
        Logger.log(`[ERROR] - ${err['message']}`, 'error');
        res.status(500).json({ reason: err['message'] });
      }
    });
  }

  private static handleCodeError(err: Error, req: express.Request, res: express.Response, next: express.NextFunction): void {
    let response: Object = {
      code: 500,
      reason: 'UNDEFINED_ERROR'
    };

    switch (err['code']) {
      case 11000:
      case 11001:
        Logger.log(`[MONGO-DuplicateKeyError] - ${err['message']}`, 'error');
        response['code'] = 400;
        response['reason'] = 'Username is already taken, please choose another one';
        break;

      case 404:
        Logger.log(`[AuthorizationError] - ${err['message']}`, 'error');
        response['code'] = 404;
        response['reason'] = 'Proper authorization on the header is required to access this resource';
        break;

      default:
        Logger.log(`[ERROR] - ${err['message']}`, 'error');
        break;
    }

    res.status(response['code']).json({ reason: response['reason'] });
  }

  private static handleNameError(err: Error, req: express.Request, res: express.Response, next: express.NextFunction): void {
    const errors: Object = err['errors'];
    let response: Object = {
      code: 500,
      reason: 'UNDEFINED_ERROR'
    };

    switch (err['name']) {
      case 'ValidationError':
        Logger.log(`[MONGO-ValidationError] - ${err['message']}`, 'error');
        response['code'] = 400;
        response['reason'] = 'Missing required fields for user creation, ' +
          'please check the response missingFields object for the fields name';

        if (errors) {
          response['missingFields'] = [];
          Object.keys(errors).forEach(errorKey => {
            if (errors[errorKey].kind === 'required') {
              if (errors[errorKey]['path'] === 'passwordHash') {
                response['missingFields'].push('password');
              } else {
                response['missingFields'].push(errors[errorKey]['path']);
              }
            }
          });
        }
        break;

      case 'CastError':
        if (err['kind'] === 'ObjectId') {
          Logger.log(`[MONGO-CastError] - ${err['message']}`, 'error');
          response['code'] = 404;
          response['reason'] = 'Id does not exist';
        }
        break;

      default:
        Logger.log(`[MONGO-ERROR] - ${err['message']}`, 'error');
        break;
    }

    res.status(response['code']).json({ reason: response['reason'], missingFields: response['missingFields'] });
  }
}
