import { default as config } from '../env/index';
import * as log4js from 'log4js';
import * as mongoAppender from 'log4js-node-mongodb';

const MONGO_URI: string = `${config.envConfig.database.MONGODB_URI}${config.envConfig.database.MONGODB_DB_MAIN}`;

log4js.addAppender(
    mongoAppender.appender({ connectionString: MONGO_URI }),
    'log'
);

const mongoLogger: any = log4js.getLogger('log');

export default class Logger {
  static log(message: string, type: string): void {

    switch (type) {
      case 'info':
        mongoLogger.info(message);
        break;
      case 'error':
        mongoLogger.error(message);
        break;
      case 'warn':
        mongoLogger.warn(message);
        break;
      case 'fatal':
        mongoLogger.fatal(message);
        break;
      default:
        break;
    }
  }
}
