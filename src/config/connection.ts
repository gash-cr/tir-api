import * as mongoose from 'mongoose';
import { default as config } from '../env/index';
import Logger from './logger';
interface connectOptions {
  autoReconnect: boolean;
  reconnectTries: number; // Never stop trying to reconnect
  reconnectInterval: number;
  loggerLevel?: string;
}
const connectOptions: connectOptions = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 1000
};

export const MONGO_URI: string = `${config.envConfig.database.MONGODB_URI}${config.envConfig.database.MONGODB_DB_MAIN}`;

export const db: mongoose.Connection = mongoose.createConnection(MONGO_URI, connectOptions);

// handlers
db.on('connecting', () => {
  Logger.log('MongoDB :: connecting', 'info');
});

db.on('error', (error) => {
  Logger.log('MongoDB :: connection' + error , 'error');
  mongoose.disconnect();
});

db.on('connected', () => {
  Logger.log('MongoDB :: connected', 'info');
});

db.once('open', () => {
  Logger.log('MongoDB :: connection opened', 'info');
});

db.on('reconnected', () => {
  Logger.log('MongoDB :: reconnected', 'warn');
});

db.on('reconnectFailed', () => {
  Logger.log('MongoDB :: reconnectFailed', 'error');
});

db.on('disconnected', () => {
  Logger.log('MongoDB :: disconnected', 'error');

});

db.on('fullsetup', () => {
  Logger.log('MongoDB :: reconnecting...', 'warn');
});
