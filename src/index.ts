import * as debug from 'debug';
import * as http from 'http';
import Server from './server';
import * as serverHandlers from './serverHandlers';
import Logger from './config/logger';

debug('ts-express:server');

const port: string | number | boolean = serverHandlers.normalizePort(process.env.PORT || 3000);

Server.set('port', port);

const server: http.Server = http.createServer(Server);

// server listen
server.listen(port, () => {
  Logger.log(`Server listening on  http://localhost:${port}`, 'info');
});

// server handlers
server.on(
  'error',
  (error) => serverHandlers.onError(error, port));
server.on(
  'listening',
  serverHandlers.onListening.bind(server));
