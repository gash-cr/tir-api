import * as connections from '../config/connection';
import { Schema, Document } from 'mongoose';

export interface IContainerModel extends Document {
  createdAt?: Date;
  updatedAt?: Date;
  containerCode: String;
  shipperName: String;
  revision: any[];
}

const ContainerSchema: Schema = new Schema({
  containerCode: {
    type: String,
    unique: true,
    required: true,
    dropDups: true
  },

  shipperName: {
    type: String
  },

  revision: [
    { type: Schema.Types.ObjectId, ref: 'Revision' }
  ],

},
  {
    collection: 'container',
    timestamps: true,
    versionKey: false
  });

ContainerSchema.pre('save', function (next: any): void {
  const container: String = this.containerCode;

  if (this.isNew && container !== '' && container !== '0') {
    this.containerCode = container.replace(/  *|-/g, '').toUpperCase();
  }

  next();

  return this;
});

export default connections.db.model<IContainerModel>('ContainerModel', ContainerSchema);
