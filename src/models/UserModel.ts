import * as connections from '../config/connection';
import { Schema, Document } from 'mongoose';
import * as bcrypt from 'bcrypt';

export interface IUserModel extends Document {
  createdAt?: Date;
  updatedAt?: Date;
  username: string;
  passwordHash: string;
  name: string;
  isAdmin: boolean;
  email?: string;
  comparePassword: Function;
  shippingCompany: string;
  isPasswordNew?: boolean;
}

const UserSchema: Schema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    dropDups: true
  },
  passwordHash: {
    type: String,
    required: true
  },
  isPasswordNew: {
    type: Boolean,
    required: false,
    default: false
  },
  name: {
    type: String,
    required: true
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  email: {
    type: String
  },
  shippingCompany: {
    type: String
  }
},
  {
    collection: 'user',
    timestamps: true,
    versionKey: false
  });

UserSchema.pre('save', function (next: any): void {
  this.passwordHash = (this.isPasswordNew) ? bcrypt.hashSync(this.passwordHash, 10) : this.passwordHash;
  next();
  return this;
});

UserSchema.methods.comparePassword = function (password: String): boolean {
  return bcrypt.compareSync(password, this.passwordHash);
};

export default connections.db.model<IUserModel>('UserModel', UserSchema);
