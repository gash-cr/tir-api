import * as connections from '../config/connection';
import { Schema, Document, PaginateModel } from 'mongoose';
import mongoosePaginate = require('mongoose-paginate-v2');

export interface Revision extends Document {

  createdAt?: Date;
  updatedAt?: Date;
  user?: string;
  shipperName?: string;
  containerCode?: string;
  boatName?: string;
  travelNumber?: string;
  shippingDetail?: string;
  shipping?: string;
  companyName?: string;
  chassis?: string;
  chassisType?: string;
  truck?: string;
  prefix?: string;
  driver?: string;
  guideNumber?: string;
  containerType?: string;
  measure?: string;
  origin?: string;
  destiny?: string;
  genSet?: Boolean;
  tiresCondition?: string;
  observations?: string;
  marks: [string, string, string, string, string, string];
  chassisDetail: [{ name: string, model: boolean }];
  tires: [string, string, string, string, string, string];
  part?: [{ name: string, in: boolean, out: boolean }];
  damages?: [{
    formName: string,
    damagedPartId: string,
    damage:
    {
      abbreviation: string,
      name: string,
      image_ref: string
    },
    coordinateX: number,
    coordinateY: number
  }];
}

const PartSchema: Schema = new Schema({
  name: String,
  in: Boolean,
  out: Boolean
}, { _id: false });

const DamageSchema: Schema = new Schema({
  formName: String,
  damagedPartId: String,
  damage: {
    abbreviation: String,
    name: String,
    image_ref: String
  },
  coordinateX: Number,
  coordinateY: Number
}, { _id: false });

const ChassisSchema: Schema = new Schema({
  name: String,
  model: Boolean,
}, { _id: false });

const GenSetSchema: Schema = new Schema({ name: String, model: Boolean });

const RevisionSchema: Schema = new Schema({
  container: {
    type: Schema.Types.ObjectId,
    ref: 'ContainerModel'
  },

  createdAt: Date,
  updatedAt: Date,
  user: String,
  shipperName: String,
  containerCode: String,
  boatName: String,
  travelNumber: String,
  shippingDetail: String,
  shipping: String,
  companyName: String,
  chassis: String,
  chassisType: String,
  truck: String,
  prefix: String,
  driver: String,
  guideNumber: String,
  containerType: String,
  measure: String,
  origin: String,
  destiny: String,
  genSet: Boolean,
  tiresCondition: String,
  observations: String,
  chassisDetail: [ChassisSchema],
  part: [PartSchema],
  damages: [DamageSchema],

  marks: {
    type: [String],
    validate: [(val: [string]): boolean => {
      return val.length <= 6;
    }, '{PATH} size exceeds the limit of 6']
  },

  tires: {
    type: [String],
    validate: [(val: [string]): boolean => {
      return val.length <= 6;
    }, '{PATH} size exceeds the limit of 6']
  },
}, {
    collection: 'revision',
    timestamps: true
  });

RevisionSchema.plugin(mongoosePaginate);
RevisionSchema.index({ createdAt: 1 });

interface RevisionModel<T extends Document> extends PaginateModel<T> {}

export default connections.db.model<Revision>('RevisionModel', RevisionSchema) as RevisionModel<Revision>;;
