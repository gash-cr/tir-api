import Server from '../server'
import * as chai from 'chai'
import * as request from 'supertest'
import { describe, it, before, after } from 'mocha'
import * as connections from '../config/connection'

const expect: Chai.ExpectStatic = chai.expect;
const user: any = {
  username: 'testAuth',
  password: '123',
  name: 'test name'
};

after(() => {
  connections.db.dropDatabase();
});

describe('Auth Test', () => {
  it('Should create a new user', (done) => {
    request(Server)
      .post('/users')
      .send(user)
      .end((err: any, res: any) => {
        expect(res.statusCode).to.be.equal(200);
        expect(res.body.username).to.equals(user.username);
        done();
      });
  });
  it('Should return a JWT', (done) => {
    request(Server)
      .post('/auth')
      .send(user)
      .end((err: any, res: any) => {
        expect(res.statusCode).to.be.equal(200);
        expect(res.body.token).to.exist;
        done();
      });
  });
});
