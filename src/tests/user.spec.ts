
import Server from '../server'
import * as chai from 'chai'
import * as request from 'supertest'
import { describe, it, before, after } from 'mocha'
import * as connections from '../config/connection'

const expect: Chai.ExpectStatic = chai.expect;

const user: any = {
  username: 'userTest',
  password: '123',
  name: 'user test',
  email: 'userTest@test.com',
  isAdmin: true,
  isPasswordNew: true,
  shippingCompany: 'Gash'
};

const changedUser = {
  _id: " ",
  username: "changedUser",
  password: " ",
  isPasswordNew: true,
  name: "changedUser",
  email: 'changedUserTest@test.com',
  isAdmin: false,
  shippingCompany: 'Gash'
};

after(() => {
  connections.db.dropDatabase();
});

describe('User Test - CREATE', () => {
  it('Should create a new user', (done) => {
    request(Server)
      .post('/users')
      .send(user)
      .end((err: any, res: any) => {
        expect(res.statusCode).to.be.equal(200);
        expect(res.body.username).to.equals(user.username);
        done();
      });
  });
});

describe('User Test - READ', () => {
  it('Should get all users from database', (done) => {
    request(Server)
      .get('/users')
      .end((err: any, res: any) => {
        expect(res.statusCode).to.be.equal(200);
        expect(res.body).to.be.an('array')
        done();
      });
  });
});

describe('User Test - UPDATE', () => {
  it('Should update an user', (done) => {
    request(Server)
      .get('/users')
      .end((err: any, res: any) => {
        expect(res.statusCode).to.be.equal(200);
        expect(res.body).to.be.an('array');
        changedUser._id = res.body[res.body.length - 1]._id;

        request(Server)
          .put('/users')
          .send(changedUser)
          .end((err: any, response: any) => {
            expect(response.statusCode).to.be.equal(200);
            done();
          });
      });
  });
});

describe('User Test - DELETE', () => {
  it('Should delete an user', (done) => {
    request(Server)
      .get('/users')
      .end((err: any, res: any) => {
        expect(res.statusCode).to.be.equal(200);
        expect(res.body).to.be.an('array');
        const idToDelete = res.body[res.body.length - 1]._id
        request(Server)
          .delete('/users')
          .send({ _id: idToDelete })
          .end((err: any, res: any) => {
            expect(res.statusCode).to.be.equal(200);
            expect(res.body._id).to.be.equal(`${idToDelete}`);
          })
        done();
      });
  });
});
