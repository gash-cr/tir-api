import Server from '../server'
import * as chai from 'chai'
import * as request from 'supertest'
import { describe, it, after } from 'mocha'
import * as connections from '../config/connection'
import { defaultRevision } from '../const/constants'

const container = { containerCode: 'test', shipperName: 'Gash', revision: defaultRevision }

const expect: Chai.ExpectStatic = chai.expect;

after(() => {
  connections.db.dropDatabase();
});

describe('Revision Test - CREATE', () => {
  it('Should create a new revision', (done) => {
    request(Server)
      .post('/container')
      .send(container)
      .end((err: any, res: any) => {
        expect(res.statusCode).to.be.equal(200);
        done();
      });
  });
});

describe('Revision Test - READ', () => {
  it('Should return all revisions', (done) => {
    request(Server)
      .get('/revision')
      .end((err: any, res: any) => {
        expect(res.statusCode).to.be.equal(200);
        done();
      });
  });
});

describe('Revision Test - READ', () => {
  it('Should return revisions by shipperName', (done) => {
    request(Server)
      .get(`/revision/by/Gash`)
      .end((err: any, res: any) => {
        expect(res.statusCode).to.be.equal(200);
        done();
      });
  });
});