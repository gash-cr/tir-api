import { IUserModel } from '../models/UserModel';

export default class UserHelper {
  static removeSensitiveData(user: IUserModel): IUserModel {
    user.passwordHash = undefined;
    user.createdAt = undefined;
    user.updatedAt = undefined;

    return user;
  }
}
